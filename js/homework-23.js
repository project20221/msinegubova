"use strict";

// Задание 1

let numStr = prompt("Введите число:");
const num = Number(numStr);
if (!numStr || isNaN(num)) {
    alert('Ошибка, вы ввели не число');
    console.log('Ошибка, вы ввели не число');
  }


const IntervalId = setInterval(() => {
  numStr--;
  console.log(`Осталось: ${numStr}`);
  if (numStr === 0) {
    clearInterval(IntervalId);
  }
}, 1000);

// Задание 2

const promise = fetch("https://reqres.in/api/users");
promise
  .then((userResponse) => {
    return userResponse.json();
  })
  .then((usersObj) => {
    return usersObj.data;
  })
  .then((users) => {
    if (users) {
      console.log(`Получили пользователей: ${users.length}`);
      users.forEach((user) => {
        console.log(`— ${user.first_name} ${user.last_name} (${user.email})`);
      });
    }
  })
  .catch(function () {
    console.log("Кажеться что-то пошло не так");
  });
