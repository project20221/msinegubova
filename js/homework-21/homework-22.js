'use strict'

import { product, reviewOne, reviewTwo } from "./data.js";


// Задание 1

const arr = [2, 8, 12, 4, 'a', {}, 122];
const sumArr = (arr) => {
    let sum = 0;
    for (let i = 0; i < arr.length; i++) {
        if (typeof arr[i] === 'number') {
            sum += arr[i];
        }
    }
    return sum;
};

console.log(`Общая сумма чисел: ${sumArr(arr)}`);


//Задание 2

console.log('Product:', product);
console.log('reviewOne:', reviewOne);
console.log('reviewTwo:', reviewTwo);

// Задание 3

let cart = [4884];
function addToCart(productId) {
    let hasInCart = cart.includes(productId);
    if (hasInCart) return;
    cart.push(productId);
}
function removeFromCart(productId) {
    cart = cart.filter(function (id) {
        return id !== productId;
    });    
}
addToCart(3456);
removeFromCart(4884);
console.log(cart);