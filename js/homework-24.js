'use strict'

let form = document.querySelector(".form");
let name = document.querySelector(".form__name");
let nameError = document.querySelector(".form__name-error");

let grade = document.querySelector(".form__grade");
let gradeError = document.querySelector(".form__grade-error");

let nameErrorStr = '';
let gradeErrorStr = '';

form.addEventListener('submit', (event) => {
    event.preventDefault();
    if (name.value === '') {
      nameErrorStr = 'Вы забыли указать имя и фамилию';
    } else if (name.value.length < 3) {
      nameErrorStr = 'Имя не может быть короче двух символов';
    } else {
      nameErrorStr = '';
    }
    if (nameErrorStr) {
    nameError.innerHTML = nameErrorStr;
    nameError.classList.add('.form__name-active');
    } else {
      nameError.innerHTML = '';
      nameError.classList.remove('.form__name-active');
    }
    if (grade.value === '') {
      gradeErrorStr = 'Поле не заполнено';
    } else if (!grade.value.match(/^\d+$/)) {
    gradeErrorStr = 'В поле введены буквы';
    } else if (!grade.value.match(/^[1-5]$/)) {
      gradeErrorStr = 'Вы ввели цифру больше 5 или меньше 1';
    } else {
      gradeErrorStr = '';
    }
    if (gradeErrorStr) {
    gradeError.innerHTML = gradeErrorStr;
    gradeError.classList.add('.form__grade-active');
    } else {
    gradeError.innerHTML = '';
    gradeError.classList.remove('.form__grade-active');
  }
});
