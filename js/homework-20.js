'use strict'

// Задание 1

for (let i = 0; i <= 20; i++) {
  if (i % 2 === 0 && i !== 0) {
    console.log(i);
  }
}

// Задание 2

let sum = 0;
let error = false;

for (let i = 0; i < 3; i++) {
  let numStr = prompt('Введите число:');
  let num = Number(numStr);

  if (!numStr || isNaN(num)) {
    alert('Ошибка, вы ввели не число');
    console.log('Ошибка, вы ввели не число');
    break;
  }
  if (num = Number(numStr)) {
    alert(`Вы ввели число: ${num}`)
    console.log(`Вы ввели число: ${num}`);
    sum += num;
  }
}
if (!error) {
    alert(`Общая сумма чисел: ${sum}`)
    console.log('Общая сумма чисел:', sum);
}

// Задание 3

const monthsArr = [
  "Январь",
  "Февраль",
  "Март",
  "Апрель",
  "Май",
  "Июнь",
  "Июль",
  "Август",
  "Сентябрь",
  "Октябрь",
  "Ноябрь",
  "Декабрь",
];
const getNameOfMonth = (monthNumber) => {
  return monthsArr[monthNumber];
};
for (let i = 0; i < monthsArr.length; i++) {
  if (getNameOfMonth(i) === "Октябрь") {
    continue;
  }
  console.log(getNameOfMonth(i));
}

console.log("Количество месяцев:", monthsArr.length);
console.log(`Выбранный месяц: ${getNameOfMonth(3)}`);